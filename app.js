const express = require('express');
const bodyParser = require('body-parser');
const Post = require('./back/models/post');
const mongoose = require('mongoose');
const app = express();
const path = require('path')



app.use(bodyParser.json());

app.use(bodyParser.urlencoded({extended:false}));

mongoose.connect("mongodb://user1:user1password@3.14.7.50:27017/sampledb?authSource=sampledb")
.then(()=> {
    console.log("Database connection successful");
})
.catch(err => {
    console.error(err);
})

app.use((req,res,next) => {
    res.setHeader("Access-Control-Allow-Origin","*");
    res.setHeader(
        "Access-Control-Allow-Headers",
        "Origin, X-Requested-With, content-type, Accept"
    );
    res.setHeader(
        "Access-Control-Allow-Methods",
        "GET, POST, PATCH, DELETE, OPTIONS"
    );


    next();
})

app.post('/api/posts',(req,res,next)=> {
    const post = new Post({
        title: req.body.title,
        content: req.body.content
    });
    //console.log(post);
    post.save()
    .then(result => {
        res.status(201).json({
            message:'Post added',
            id: result._id
        })
    });
    
    
})

app.get('/api/posts',(req,res,next)=> {
    Post.find()
    .then(docs => {
        //console.log(docs);
        res.status(200).json({
            message : 'Sent successfully',
            posts : docs
        });
    })
    .catch(err => {
        console.error(err);
    })
});

app.delete('/api/posts/:id',(req,res,next) => {
    console.log(req.params.id)
    Post.deleteOne({_id: req.params.id})
    .then(result => {
        console.log('Deleted');
        res.status(200).json({
            message: 'Post Deleted'
        });
    })
    .catch(err => {
        console.error(err);
    })
});
app.use("/", express.static(path.join(__dirname,"angular_final")));
app.use((req,res,next)=>{
    res.sendFile(path.join(__dirname,"angular_final","index.html"))
});


module.exports=app;