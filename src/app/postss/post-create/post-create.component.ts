import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { style } from '@angular/animations';
import { Post } from '../post.model';
import { PostsService } from '../posts.service';



@Component({
  selector: 'app-post-create',
  templateUrl: './post-create.component.html',
  styleUrls: ['./post-create.component.css']
})
export class PostCreateComponent {

  enteredTitle = '';
  enteredContent = '';


  constructor(public postsService: PostsService) {}


  onAddPost(Form: NgForm) {
    if (Form.invalid) {
      return;
    }
    const post: Post = {
      id: null,
      title: Form.value.title,
      content: Form.value.content
    };
    this.postsService.addPosts(post.title,post.content);
    Form.resetForm();
  }
}
