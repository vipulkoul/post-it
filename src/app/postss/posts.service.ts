import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';
import { map } from 'rxjs/operators';

import { Post } from './post.model';

@Injectable({ providedIn: 'root' })
export class PostsService {
  private posts: Post[] = [];
  private postsUpdated = new Subject<Post[]>();

  constructor(private http: HttpClient) {}

  getPosts() {
    this.http
    .get<{ message: string, posts: any }>('http://13.58.175.188:3000/api/posts')
    .pipe(map((postData) => {
      return postData.posts.map(post => {
        return {
         title: post.title,
         content: post.content,
         id: post._id
        };
      });
    }))
    .subscribe(post => {
      this.posts = post;
      this.postsUpdated.next([...this.posts]);
    });
  }

  getPostsUpdated() {
    return this.postsUpdated.asObservable();
  }

  addPosts(title: string, content: string) {
    const post: Post = { id: null, title: title, content: content };
    this.http.post<{ message: string , id: string }>('http://13.58.175.188:3000/api/posts', post)
    .subscribe(responseData => {
      console.log(responseData.message);
      post.id = responseData.id;
      this.posts.push(post);
      this.postsUpdated.next([...this.posts]);
    });
  }

  deletePost(postId: string) {
    console.log(postId)
    this.http.delete<{message: string}>('http://13.58.175.188:3000/api/posts/' + postId)
    .subscribe((resData) => {
      console.log(resData.message);
      this.posts = this.posts.filter(post => post.id !== postId);
      this.postsUpdated.next([...this.posts]);
    });
  }
}
