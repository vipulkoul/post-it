import { Component, OnInit, OnDestroy } from '@angular/core';
import { Post } from '../post.model';
import { PostsService } from '../posts.service';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.css']
})
export class PostListComponent implements OnInit, OnDestroy{
  // posts=[
  //  {title:'First',content:'First Post'},
  //  {title:'Second',content:'Second Post'},
  //  {title:'Third',content:'Third Post'}
  //]
  posts: Post[] = [];

  constructor(public postsService: PostsService) {}

  private postsSub: Subscription;

  ngOnInit() {
    this.postsService.getPosts();
    this.postsSub = this.postsService.getPostsUpdated()
      .subscribe((post: Post[]) => {
        this.posts = post;
      });
  }


  onDelete(postId: string) {
    console.log(postId);
    this.postsService.deletePost(postId);
  }


  ngOnDestroy() {
    this.postsSub.unsubscribe();
  }
}
